## Introduction

This is an example package for the Interactive Broker Api Library (Version 9.71) that is given at http://interactivebrokers.github.io .

The original IB Api Library contains a lot of other information which makes it difficult to understand and extract the library for use.

The aim of this package is to provide a better and cleaner package for new user to understand and use the IB Api Library.

## Creating Library Jar
To compile the library jar, run the command 'mvn clean package', and the standalone IB Api Library jar will be located at the target folder with the name 'ib-api-javaclient-9.71.jar'

You can then include the jar in your project and start using it.